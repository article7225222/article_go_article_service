
CREATE TABLE IF NOT EXISTS "articlies" (
    "id" uuid PRIMARY KEY,
    "text" VARCHAR NOT NULL,
    "user_id" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT (now()),
    "updated_at" TIMESTAMP DEFAULT (now())
);
