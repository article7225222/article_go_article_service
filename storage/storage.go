package storage

import (
	"context"
	"ibron/ibron_go_user_service.git/genproto/article_service"
	"ibron/ibron_go_user_service.git/models"
)

type StorageI interface {
	CloseDB()
	Article() ArticleRepoI
}

type ArticleRepoI interface {
	Create(ctx context.Context, req *article_service.CreateArticle) (resp *article_service.ArticlePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *article_service.ArticlePrimaryKey) (resp *article_service.Article, err error)
	GetAll(ctx context.Context, req *article_service.GetListArticleRequest) (resp *article_service.GetListArticleResponse, err error)
	Update(ctx context.Context, req *article_service.UpdateArticle) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *article_service.ArticlePrimaryKey) error
}
