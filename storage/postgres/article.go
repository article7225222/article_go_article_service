package postgres

import (
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"ibron/ibron_go_user_service.git/genproto/article_service"
	"ibron/ibron_go_user_service.git/models"
	"ibron/ibron_go_user_service.git/storage"

	"ibron/ibron_go_user_service.git/pkg/helper"
)

type ArticleRepo struct {
	db *pgxpool.Pool
}

func NewArticleRepo(db *pgxpool.Pool) storage.ArticleRepoI {
	return &ArticleRepo{
		db: db,
	}
}

func (c *ArticleRepo) Create(ctx context.Context, req *article_service.CreateArticle) (resp *article_service.ArticlePrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "articlies" (
				id,
				text,
				user_id,
				updated_at
			) VALUES ($1, $2, $3, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Text,
		req.UserId,
	)

	if err != nil {
		return nil, err
	}

	return &article_service.ArticlePrimaryKey{UserId: id.String()}, nil
}

func (c *ArticleRepo) GetByPKey(ctx context.Context, req *article_service.ArticlePrimaryKey) (*article_service.Article, error) {

	query := `
		SELECT
			id,
			COALESCE(text, ''),
			COALESCE(user_id, ''),
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "articlies"
		WHERE id = $1
	`
	var resp article_service.Article

	err := c.db.QueryRow(ctx, query, req.GetUserId()).Scan(
		&resp.ArticleId,
		&resp.Text,
		&resp.UserId,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *ArticleRepo) GetAll(ctx context.Context, req *article_service.GetListArticleRequest) (resp *article_service.GetListArticleResponse, err error) {

	resp = &article_service.GetListArticleResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			text,
			user_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "articlies"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE first_name||' '||last_name ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var article article_service.Article

		err := rows.Scan(
			&resp.Count,
			&article.ArticleId,
			&article.Text,
			&article.UserId,
			&article.CreatedAt,
			&article.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Articlies = append(resp.Articlies, &article)
	}

	return
}

func (c *ArticleRepo) Update(ctx context.Context, req *article_service.UpdateArticle) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "articlies"
			SET
				text = :text,
				user_id = :user_id,

				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":      req.GetArticleId(),
		"text":    req.GetText(),
		"user_id": req.GetUserId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ArticleRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"articlies"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *ArticleRepo) Delete(ctx context.Context, req *article_service.ArticlePrimaryKey) error {

	query := `DELETE FROM "articlies" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.UserId)

	if err != nil {
		return err
	}

	return nil
}
