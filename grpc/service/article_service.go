package service

import (
	"context"
	"ibron/ibron_go_user_service.git/config"
	"ibron/ibron_go_user_service.git/genproto/article_service"
	"ibron/ibron_go_user_service.git/grpc/client"
	"ibron/ibron_go_user_service.git/models"
	"ibron/ibron_go_user_service.git/pkg/logger"
	"ibron/ibron_go_user_service.git/storage"

	"github.com/golang/protobuf/ptypes/empty"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ArticleService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*article_service.UnimplementedArticleServiceServer
}

func NewArticleService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ArticleService {
	return &ArticleService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ArticleService) Create(ctx context.Context, req *article_service.CreateArticle) (resp *article_service.Article, err error) {

	i.log.Info("---CreateArticle------>", logger.Any("req", req))

	pKey, err := i.strg.Article().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateArticle->Article->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Article().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyArticle->Article->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ArticleService) GetById(ctx context.Context, req *article_service.ArticlePrimaryKey) (resp *article_service.Article, err error) {

	i.log.Info("---GetArticleByID------>", logger.Any("req", req))

	resp, err = i.strg.Article().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetArticleByID->Article->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ArticleService) GetList(ctx context.Context, req *article_service.GetListArticleRequest) (resp *article_service.GetListArticleResponse, err error) {

	i.log.Info("---GetArticles------>", logger.Any("req", req))

	resp, err = i.strg.Article().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetArticles->Article->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ArticleService) Update(ctx context.Context, req *article_service.UpdateArticle) (resp *article_service.Article, err error) {

	i.log.Info("---UpdateArticle------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Article().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateArticle--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Article().GetByPKey(ctx, &article_service.ArticlePrimaryKey{UserId: req.ArticleId})
	if err != nil {
		i.log.Error("!!!GetArticle->Article->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ArticleService) UpdatePatch(ctx context.Context, req *article_service.UpdatePatchArticle) (resp *article_service.Article, err error) {

	i.log.Info("---UpdatePatchEnrolledStudent------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Article().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchArticle--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Article().GetByPKey(ctx, &article_service.ArticlePrimaryKey{UserId: req.Id})
	if err != nil {
		i.log.Error("!!!GetArticle->Article->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ArticleService) Delete(ctx context.Context, req *article_service.ArticlePrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteArticle------>", logger.Any("req", req))

	err = i.strg.Article().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteArticle->Article->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
