package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"ibron/ibron_go_user_service.git/config"
	"ibron/ibron_go_user_service.git/genproto/article_service"
	"ibron/ibron_go_user_service.git/grpc/client"
	"ibron/ibron_go_user_service.git/grpc/service"
	"ibron/ibron_go_user_service.git/pkg/logger"
	"ibron/ibron_go_user_service.git/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	article_service.RegisterArticleServiceServer(grpcServer, service.NewArticleService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
